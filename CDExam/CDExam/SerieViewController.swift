//
//  SerieViewController.swift
//  CDExam
//
//  Created by Alexandre Martinez Olmos on 12/2/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit
import CoreData

class SerieViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var genreTextField: UITextField!
    @IBOutlet weak var seasonsTextField: UITextField!
    
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    
    var tit: String = ""
    var gnr: String = ""
    var sns: String = ""
    
    var existingSerie: NSManagedObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (existingSerie != nil) {
            titleTextField.text = tit
            genreTextField.text = gnr
            seasonsTextField.text = sns
        }
    }

    @IBAction func saveAction(sender: AnyObject) {
        if (titleTextField.text.isEmpty ||
            genreTextField.text.isEmpty ||
            seasonsTextField.text.isEmpty)
        {
            let alertController = UIAlertController(title: "Error saving Serie", message:
                "You have to add all fields!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
            
        else {
            //Reference to our app delegate
            let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            //Reference MOC
            let contxt: NSManagedObjectContext = appDel.managedObjectContext!
            let en = NSEntityDescription.entityForName("Serie", inManagedObjectContext: contxt)
            
            //Check if item exists
            if (existingSerie != nil) {
                existingSerie.setValue(titleTextField.text as String, forKey: "title")
                existingSerie.setValue(genreTextField.text as String, forKey: "genre")
                existingSerie.setValue(seasonsTextField.text as String, forKey: "seasons")
            } else {
                //Create instance of our data model and initialize
                var newSerie = Serie(entity: en!, insertIntoManagedObjectContext: contxt)
                
                //Map our properties
                newSerie.title = titleTextField.text
                newSerie.genre = genreTextField.text
                newSerie.seasons = seasonsTextField.text
                
                println(newSerie)
            }
            
            //Save our context
            contxt.save(nil)
        }
        
        //Navigate back to root VC
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        //Navigate back to root VC
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
