//
//  Serie.swift
//  CDExam
//
//  Created by Alexandre Martinez Olmos on 12/2/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit
import CoreData

@objc(Serie)
class Serie: NSManagedObject {
   //Properties feeding the atributes in our entity. Must match the entity atributes
    @NSManaged var title: String
    @NSManaged var genre: String
    @NSManaged var seasons: String
}
