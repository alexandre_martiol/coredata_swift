//
//  ListTableViewController.swift
//  CDExam
//
//  Created by Alexandre Martinez Olmos on 12/2/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit
import CoreData

class ListTableViewController: UITableViewController {
    var seriesList: Array<AnyObject> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(animated: Bool) {
        //Reference to our app delegate
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        //Reference MOC
        let contxt: NSManagedObjectContext = appDel.managedObjectContext!
        let freq = NSFetchRequest(entityName: "Serie")
        
        seriesList = contxt.executeFetchRequest(freq, error: nil)!
        tableView.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "update" {
            var selectedSerie: NSManagedObject = seriesList[self.tableView.indexPathForSelectedRow()!.row] as! NSManagedObject
            
            let IVC: SerieViewController = segue.destinationViewController as! SerieViewController
            
            IVC.tit = selectedSerie.valueForKey("title") as! String
            IVC.gnr = selectedSerie.valueForKey("genre") as! String
            IVC.sns = selectedSerie.valueForKey("seasons") as! String
            IVC.existingSerie = selectedSerie
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return seriesList.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Configure the cell...
        let CellID = "Cell"
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(CellID) as! UITableViewCell
        
        if let ip = indexPath as NSIndexPath? {
            var data: NSManagedObject = seriesList[ip.row] as! NSManagedObject
            cell.textLabel?.text = data.valueForKeyPath("title") as? String
            
            var gnr = data.valueForKeyPath("genre") as! String
            var sns = data.valueForKeyPath("seasons") as! String
            cell.detailTextLabel!.text = "Genre: \(gnr) / Seasons: \(sns)"
        }

        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //Reference to our app delegate
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        //Reference MOC
        let contxt: NSManagedObjectContext = appDel.managedObjectContext!
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            if let tv = tableView as UITableView? {
                contxt.deleteObject(seriesList[indexPath.row] as! NSManagedObject)
                seriesList.removeAtIndex(indexPath.row)
                tv.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            }
            
            var error: NSError? = nil
            if !contxt.save(&error) {
                abort()
            }
        }
    }
    
    //Functions that edit the items in TableView
    @IBAction func editAction(sender: AnyObject) {
        if (self.editing == false) {
            self.editing = true
        } else {self.editing = false}
    }
    
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true // Yes, the table view can be reordered
    }
    
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        //Update the item in my data source by first removing at the from index, then inserting at the to index.
        let item: AnyObject = seriesList[fromIndexPath.row]
        seriesList.removeAtIndex(fromIndexPath.row)
        seriesList.insert(item, atIndex: toIndexPath.row)
    }
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
